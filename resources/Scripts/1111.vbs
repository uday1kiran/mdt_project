Option Explicit 'force all variables to be declared

Const ForWriting = 2

Dim env

Set env = CreateObject("Microsoft.SMS.TSEnvironment")
MsgBox("Project code retrieval")
MsgBox(env("MyCustomVariable"))

Dim objFSO,ss

Set objFSO = CreateObject("Scripting.FileSystemObject")

ss = "Folderset:"
Recurse objFSO.GetFolder("X:\")
Msgbox(ss)

Sub Recurse(objFolder)
    Dim objFile, objSubFolder

    For Each objFile In objFolder.Files
        If LCase(objFSO.GetExtensionName(objFile.Name)) = "vbs" Then
            ss = ss & ";" & objfile.Path
        End If
    Next

    For Each objSubFolder In objFolder.SubFolders
        Recurse objSubFolder
    Next
End Sub